/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Guillermo
 */
public class Tarea_ParticipanteDTO {
    private String idtareaparticipante;
    private int idtarea;
    private int codigoparticipante;
    
    
    public Tarea_ParticipanteDTO(int idtarea,int codigoparticipante){
     
     this.idtareaparticipante = idtarea+"-"+codigoparticipante;
     this.codigoparticipante = codigoparticipante;
     this.idtarea = idtarea;
    }

    public String getIdtarea_participante() {
        return idtareaparticipante;
    }

    public void setIdTarea_participante(String tarea_participante) {
        this.idtareaparticipante = tarea_participante;
    }

    public int getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(int idtarea) {
        this.idtarea = idtarea;
    }

    public int getCodigoparticipante() {
        return codigoparticipante;
    }

    public void setCodigoparticipante(int codigoparticipante) {
        this.codigoparticipante = codigoparticipante;
    }
    
}
