/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
/**
 *
 * @author Guillermo
 */
public class Proyecto_ParticipanteDTO {
    
    private String idproyecto_participante;
    private int idproyecto;
    private int codigoparticipante;
    
    
    public Proyecto_ParticipanteDTO(int idproyecto,int codigoparticipante){
     
        this.idproyecto_participante = idproyecto+"-"+codigoparticipante;
        this.codigoparticipante = codigoparticipante;
        this.idproyecto = idproyecto;
    }

    public String getIdproyecto_participante() {
        return idproyecto_participante;
    }

    public void setIdProyecto_participante(String idproyecto_participante) {
        this.idproyecto_participante = idproyecto_participante;
    }

    public int getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(int idproyecto) {
        this.idproyecto = idproyecto;
    }

    public int getCodigoparticipante() {
        return codigoparticipante;
    }

    public void setCodigoparticipante(int codigoparticipante) {
        this.codigoparticipante = codigoparticipante;
    }
    
   
}
