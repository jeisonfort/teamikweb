/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Datos.ProyectoDAO;
import Datos.ProyectoDAOJDBC;
import Datos.TareaDAO;
import Datos.TareaDAOJDBC;
import Modelo.ProyectoDTO;
import Modelo.TareaDTO;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jeiso
 */
@WebServlet("/proyecto")
public class ProyectosController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
       
        String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "ir":
            
                this.listarTareas(request, response);
            
                break;
             case "listar":
            
                this.listarProyectos(request, response);
            
                break;

            default:
                 listarProyectos(request, response);
                break;
        }
        }
    }
    private void listarProyectos(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        ProyectoDAO proyecto = new ProyectoDAOJDBC();
        List<ProyectoDTO> proyectos  = proyecto.consultar();
        request.getSession().setAttribute("proyectos2", proyectos);
//        request.getRequestDispatcher("vista/listaempresas.jsp").forward(request, response);
        response.sendRedirect("dist/index.jsp");
    }
     @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
       
        String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "ir":
            
                this.listarTareas(request, response);
            
                break;

            case "creartarea":
                this.crearTarea(request, response);
                break;
            case "crearproyecto":
                
                break;
            default:
                response.sendRedirect("dist/inicio.jsp");
                break;
        }
        }
    }
    private void listarTareas (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        int idP = Integer.parseInt(request.getParameter("idproyecto"));
        String nombreP = request.getParameter("nombre");
        ProyectoDAO pro = new ProyectoDAOJDBC();
        ProyectoDTO proy= pro.buscar(new ProyectoDTO(idP));
        String descripcion = proy.toString();
        TareaDAO t = new TareaDAOJDBC();
        List<TareaDTO> tareas = t.buscarTareas(idP);
        List<String> nombres=new ArrayList<>();
        nombres.add(nombreP);
        request.getSession().setAttribute("nombres", nombreP);
        request.getSession().setAttribute("descripcion", descripcion);
        request.getSession().setAttribute("idProyect", idP);
        if(tareas.isEmpty())response.sendRedirect("dist/inicio.jsp");
        else{
        request.getSession().setAttribute("tareas", tareas);
        response.sendRedirect("dist/inicio.jsp");
        }
    }
    private void crearTarea(HttpServletRequest request, HttpServletResponse response){
        
    }
    /**
    private void crearProyecto(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        ProyectoDAO proyecto = new ProyectoDAOJDBC();
        int idP = Integer.parseInt(request.getParameter("idproyecto"));
        String nombre =request.getParameter("nombre");
       
        String fechaIni = request.getParameter("fechaini");
        String fecha[]= fechaIni.split("-");
         Date fechaI = new Date(idP, idP, idP)
        ProyectoDTO nuevo = new ProyectoDTO(idP,
                nombre, 
                , 
                request.getParameter("fechafin"));
    }
    * **/
    
    
}
