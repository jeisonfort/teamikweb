/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author jeiso
 */
public class TareaDTO {
    protected int idtarea;
    protected int idProyecto;

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getEvidencia() {
        return evidencia;
    }

    public void setEvidencia(String evidencia) {
        this.evidencia = evidencia;
    }
    protected String evidencia;
    

    public int getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(int idtarea) {
        this.idtarea = idtarea;
    }
    protected Date fechaInicio;
    protected Date fechaFin;
    protected String nombre;
    protected boolean activa;
    protected boolean entregada;
    
    public TareaDTO (){
    }
    public TareaDTO(int idTarea, String pFechaInicio, String pFechaFin,String entregada, String activa, String evidencia, String nombre){
        this.idtarea=idTarea;
        this.nombre=nombre;
        String fechaIn[] = pFechaInicio.split("-");
        this.fechaInicio=new Date (Integer.parseInt(fechaIn[0]),Integer.parseInt(fechaIn[1]),Integer.parseInt(fechaIn[2]));
        fechaIn= pFechaFin.split("-");
        this.fechaFin=new Date (Integer.parseInt(fechaIn[0]),Integer.parseInt(fechaIn[1]),Integer.parseInt(fechaIn[2]));
        if(activa.equals("1"))this.activa=true;
        else this.activa=false;
        if(entregada.equals("1"))this.entregada=true;
        else this.entregada=false;
        
        this.evidencia=evidencia;
    }
     public TareaDTO(int idTarea, Date pFechaInicio, Date pFechaFin,boolean entregada, boolean activa, String evidencia, String nombre, int pIdProyecto){
        this.idtarea=idTarea;
        this.nombre=nombre;
        this.fechaInicio=pFechaInicio;
        this.fechaFin=pFechaFin;
        this.activa=activa;
        this.entregada=entregada;
        this.evidencia=evidencia;
        this.idProyecto=pIdProyecto;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public boolean isEntregada() {
        return entregada;
    }

    public void setEntregada(boolean entregada) {
        this.entregada = entregada;
    }
    
    public String entregada(){
     return (isEntregada())? "1":"0";
    }
    public String activa(){
        return (isActiva())?"1":"0";
    }
    
    @Override
    public String toString(){
        return "Tarea "+this.idtarea+":"+ " "+this.nombre+" , "+ this.fechaInicio.toString()
                +" , "+this.fechaFin.toString()+" , "+this.idProyecto;
    }
    
    
}
