/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.ParticipanteDTO;
import Modelo.ProyectoDTO;
import Modelo.Proyecto_ParticipanteDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guillermo
 */
public class Proyecto_ParticipanteDAOJDBC implements Proyecto_ParticipanteDAO{
    
    
    
    
    public static final String SQL_INSERTAR = "INSERT INTO proyecto_participante (idproyectoparticipante,idproyecto,codigoparticipante) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT idproyectoparticipante,idproyecto,codigoparticipante FROM proyecto_participante";
    public static final String SQL_UPDATE = " UPDATE proyecto_participante SET idproyecto=?,codigoparticipante=? WHERE idproyectoparticipante =? ";
    public static final String SQL_DELETE = " DELETE FROM proyecto_participante WHERE idproyectoparticipante=?";

    
    
    
    
    
    @Override
    public List<Proyecto_ParticipanteDTO> consultar() {
       Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        Proyecto_ParticipanteDTO pp = null;
        List<Proyecto_ParticipanteDTO> listpp = new ArrayList<>();

        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()){
                int idproyecto = res.getInt("idproyecto");
                int codigoparticipante = res.getInt("codigoparticipante");
                String idproyectoparticipante = res.getString("idproyectoparticipante");
                
                pp = new Proyecto_ParticipanteDTO(idproyecto, codigoparticipante);
                listpp.add(pp);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Proyecto_ParticipanteDTO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);
                
            } catch (SQLException ex) {
                Logger.getLogger(Proyecto_ParticipanteDTO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listpp;
    }
    

    @Override
    public int crear(Proyecto_ParticipanteDTO pp) {
       Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_INSERTAR);
            sen.setString(1, pp.getIdproyecto_participante());
            sen.setInt(2, pp.getIdproyecto());
            sen.setInt(3, pp.getCodigoparticipante());
            
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    
    }
    

    @Override
    public int actualizar(Proyecto_ParticipanteDTO pp) {
       Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_UPDATE);
           
            sen.setInt(1, pp.getIdproyecto());
            sen.setInt(2, pp.getCodigoparticipante());
            sen.setString(3, pp.getIdproyecto_participante());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }

    @Override
    public int eliminar(Proyecto_ParticipanteDTO pp) {
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_DELETE);
            sen.setString(1, pp.getIdproyecto_participante());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }
    
    } 

    
  
    
  

