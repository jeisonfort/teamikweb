/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.ProyectoDTO;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public interface ProyectoDAO {
    public List<ProyectoDTO> consultar();
    public int crear(ProyectoDTO proyectoDTO);
    public int actualizar(ProyectoDTO proyectoDTO);
    public int eliminar(ProyectoDTO proyectoDTO);
    public ProyectoDTO buscar(ProyectoDTO p);
}
