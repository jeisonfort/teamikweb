/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Datos.ParticipanteDAO;
import Datos.ParticipanteDAOJDBC;
import Datos.ProyectoDAO;
import Datos.ProyectoDAOJDBC;
import Modelo.ParticipanteDTO;
import Modelo.ProyectoDTO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jeiso
 */
@WebServlet("/login")
public class ParticipanteController extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
         String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "crear":      
                    this.crearParticipante(request, response);              
                break;


            default:
                response.sendRedirect("dist/index.jsp");
                break;
        }
        }
        else  response.sendRedirect("dist/login.jsp");
    }
    private void login(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        ParticipanteDAO participante = new ParticipanteDAOJDBC();
        ProyectoDAO pDao= new ProyectoDAOJDBC();
        List<ProyectoDTO> proyectos = pDao.consultar();
        String correo = request.getParameter("correoL");
        String clave = request.getParameter("claveL");
        ParticipanteDTO p = participante.buscarLogin(new ParticipanteDTO(correo, clave));
        if(p.getCorreo().equals("INVALIDO"))response.sendRedirect("dist/login.jsp");
        else{
           request.getSession().setAttribute("participante", p);
           request.getSession().setAttribute("proyectos", proyectos);
           request.getSession().setAttribute("nombrePart", p.getNombre());        
           response.sendRedirect("dist/index.jsp");
        }
    }
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        
        String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "iniciar":      
                    this.login(request, response);              
                break;


            default:
                response.sendRedirect("dist/index.jsp");
                break;
        }
        }
        else  response.sendRedirect("dist/login.jsp");
    }
    
    private void crearParticipante(HttpServletRequest request, HttpServletResponse response){
       
    }
    
}
