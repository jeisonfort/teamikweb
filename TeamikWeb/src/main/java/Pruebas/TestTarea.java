/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pruebas;

import Datos.ProyectoDAOJDBC;
import Datos.TareaDAO;
import Datos.TareaDAOJDBC;
import Modelo.TareaDTO;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author jeiso
 */
public class TestTarea {
    public static void main (String arg[]){
        TareaDAO tarea = new TareaDAOJDBC();
        TareaDTO nuva = new TareaDTO();
        ProyectoDAOJDBC p = new ProyectoDAOJDBC();
        nuva.setNombre("Prueba Relacion 3");
        nuva.setEntregada(false);
        nuva.setActiva(true);
        nuva.setFechaInicio(new Date(2020-1900, 10, 1));
        nuva.setFechaFin(new Date (2020-1900,10,2));
        nuva.setEvidencia("AASASAS");
        nuva.setIdProyecto(181127);
        tarea.crear(nuva,p.consultar().get(0));
        
        for (TareaDTO tareaDTO : tarea.consultar()) {
            System.out.println("Tarea : "+ tareaDTO.toString());
        }
        //nuva = tarea.consultar().get(0);
        //nuva.setNombre("Prueba Actu");
        //tarea.update(nuva);
        //tarea.delete(nuva);
        System.out.println("*****************************");
        for (TareaDTO tareaDTO : tarea.buscarTareas(124253)) {
            System.out.println("Tarea : "+ tareaDTO.toString());
        }
        
        
    
    }
}
