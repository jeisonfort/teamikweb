/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Tarea_ParticipanteDTO;
import java.util.List;

/**
 *
 * @author Guillermo
 */
public interface Tarea_ParticipanteDAO {
    
   public List<Tarea_ParticipanteDTO> consultar();
    public int crear(Tarea_ParticipanteDTO tp);
    public int actualizar(Tarea_ParticipanteDTO tp);
    public int eliminar(Tarea_ParticipanteDTO tp);
    
   
    
}
