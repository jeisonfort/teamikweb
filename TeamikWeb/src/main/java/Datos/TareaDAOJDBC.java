/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import static Datos.ParticipanteDAOJDBC.SQL_CONSULTA_LOGIN;
import Modelo.ParticipanteDTO;
import Modelo.ProyectoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import Modelo.TareaDTO;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeiso
 */
public class TareaDAOJDBC implements TareaDAO{
    public static final String SQL_INSERTAR= "INSERT INTO tarea (idtarea, fechainicio, fechafin, entregada, activa, evidencia, nombre,idproyecto) VALUES (?,?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT idtarea, fechainicio, fechafin, entregada, activa, evidencia, nombre, idproyecto FROM tarea";
    public static final String SQL_UPDATE ="UPDATE tarea SET fechainicio= ?, fechafin= ?, entregada= ?, activa= ?, evidencia= ?, nombre= ? WHERE idTarea = ?";
    public static final String SQL_DELETE ="DELETE FROM tarea WHERE idTarea = ?";
    public static final String SQL_CONSULTA_ID_P = "SELECT idtarea, fechainicio, fechafin, entregada, activa, evidencia, nombre  FROM tarea WHERE idproyecto= ?";

     public int crear(TareaDTO t, ProyectoDTO p){
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_INSERTAR);
            sen.setInt(1, t.getIdtarea());
            sen.setDate(2,t.getFechaInicio());
            sen.setDate(3, t.getFechaFin());
            sen.setBoolean(4,t.isEntregada());
            sen.setBoolean(5,t.isActiva());
            sen.setString(6, t.getEvidencia());
            sen.setString(7, t.getNombre());
            sen.setObject(8, p.getIdproyecto());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    
    }
      public List<TareaDTO> consultar() {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        TareaDTO tarea = null;
        List<TareaDTO> tareas = new ArrayList<>();

        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()){
                int id = res.getInt("idtarea");
                Date fechaIni = res.getDate("fechainicio");
                Date fechaFin = res.getDate("fechafin");
                boolean entregada = res.getBoolean("entregada");
                boolean activa = res.getBoolean("activa");
                String evidencia = res.getString("evidencia");
                String nombre = res.getString("nombre");
                int proyecto = res.getInt("idproyecto");
                tarea = new TareaDTO(id,fechaIni,fechaFin,entregada,activa,evidencia,nombre,proyecto);
               
                tareas.add(tarea);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TareaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);
                
            } catch (SQLException ex) {
                Logger.getLogger(TareaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return tareas;
    }
      public int update (TareaDTO t){
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_UPDATE);
            
            
            sen.setDate(1,t.getFechaInicio());
            sen.setDate(2, t.getFechaFin());
            sen.setBoolean(3,t.isEntregada());
            sen.setBoolean(4,t.isActiva());
            sen.setString(5, t.getEvidencia());
            sen.setString(6, t.getNombre());
            sen.setInt(7, t.getIdtarea());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }
       public int delete (TareaDTO t){
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_DELETE);
            sen.setInt(1, t.getIdtarea());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }
     
    @Override
    public List<TareaDTO> buscarTareas(int id)  {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        TareaDTO tarea = null;
         List<TareaDTO> tareas = new ArrayList<>();
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA_ID_P);
            sen.setInt(1, id);
            res = sen.executeQuery();
            while(res.next()){
                int idT = res.getInt("idtarea");
                Date fechaIni = res.getDate("fechainicio");
                Date fechaFin = res.getDate("fechafin");
                boolean entregada = res.getBoolean("entregada");
                boolean activa = res.getBoolean("activa");
                String evidencia = res.getString("evidencia");
                String nombre = res.getString("nombre");
              
                tarea = new TareaDTO(idT,fechaIni,fechaFin,entregada,activa,evidencia,nombre,id);
               
                tareas.add(tarea);
                
            }
            
           
            
                
            
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return tareas;
    }
   
}
