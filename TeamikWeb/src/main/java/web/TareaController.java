/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Datos.ParticipanteDAO;
import Datos.ParticipanteDAOJDBC;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jeiso
 */
@WebServlet("/tarea")
public class TareaController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "crear":      
                crearTarea(request, response);
                break;


            default:
                response.sendRedirect("dist/index.jsp");
                break;
        }
        }
        else  response.sendRedirect("dist/login.jsp");
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        
        String accion = request.getParameter("accion");
        if(accion!=null){
        switch(accion){
            case "crear":      
                crearTarea(request, response);
                break;


            default:
                response.sendRedirect("dist/index.jsp");
                break;
        }
        }
        else  response.sendRedirect("dist/login.jsp");
    }
    private void crearTarea(HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException{
        int idp = Integer.parseInt(request.getParameter("idproyecto"));
        ParticipanteDAO part = new ParticipanteDAOJDBC();
        request.setAttribute("idp", idp);
        request.setAttribute("participantesTarea", part.consultar());
        response.sendRedirect("dist/tareas.jsp");
        
        
    }
    
    
}
