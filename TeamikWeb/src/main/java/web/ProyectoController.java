/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Datos.ProyectoDAO;
import Datos.ProyectoDAOJDBC;
import Modelo.ProyectoDTO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jeiso
 */
@WebServlet("/proyect")
public class ProyectoController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        listarProyectos(request, response);
    }
    private void listarProyectos(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        ProyectoDAO proyecto = new ProyectoDAOJDBC();
        List<String> participante = new ArrayList() ; 
        participante.add((String) request.getAttribute("participante").toString());
        List<ProyectoDTO> proyectos  = proyecto.consultar();
        request.getSession().setAttribute("proyectos", proyectos);
        request.getSession().setAttribute("codigoP", participante);
//        request.getRequestDispatcher("vista/listaempresas.jsp").forward(request, response);
        response.sendRedirect("dist/index.jsp");
    }
    
}
