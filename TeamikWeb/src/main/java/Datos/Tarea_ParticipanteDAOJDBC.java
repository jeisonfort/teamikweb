/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Proyecto_ParticipanteDTO;
import Modelo.Tarea_ParticipanteDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guillermo
 */
public class Tarea_ParticipanteDAOJDBC implements Tarea_ParticipanteDAO{
 
     public static final String SQL_INSERTAR = "INSERT INTO tarea_participante (idtareaparticipante,idtarea,codigoparticipante) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT idtareaparticipante,idtarea,codigoparticipante FROM tarea_participante";
    public static final String SQL_UPDATE = " UPDATE tarea_participante SET idtarea=?,codigoparticipante=? WHERE idtareaparticipante =? ";
    public static final String SQL_DELETE = " DELETE FROM tarea_participante WHERE idtareaparticipante=?";
     
    @Override
    public List<Tarea_ParticipanteDTO> consultar() {
        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        Tarea_ParticipanteDTO tp = null;
        List<Tarea_ParticipanteDTO> listtp = new ArrayList<>();

        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();
            while (res.next()){
                int idtarea = res.getInt("idtarea");
                int codigoparticipante = res.getInt("codigoparticipante");
                String idtareaparticipante = res.getString("idtareaparticipante");
                
                tp = new Tarea_ParticipanteDTO(idtarea, codigoparticipante);
                listtp.add(tp);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Tarea_ParticipanteDTO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);
                
            } catch (SQLException ex) {
                Logger.getLogger(Tarea_ParticipanteDTO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listtp;
    }

    @Override
    public int crear(Tarea_ParticipanteDTO tp) {
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_INSERTAR);
            sen.setString(1, tp.getIdtarea_participante());
            sen.setInt(2, tp.getIdtarea());
            sen.setInt(3, tp.getCodigoparticipante());
            
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }

    @Override
    public int actualizar(Tarea_ParticipanteDTO tp) {
         Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_UPDATE);
           
            sen.setInt(1, tp.getIdtarea());
            sen.setInt(2, tp.getCodigoparticipante());
            sen.setString(3, tp.getIdtarea_participante());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }

    @Override
    public int eliminar(Tarea_ParticipanteDTO tp) {
        Connection con =null;
        PreparedStatement sen =null;
        int registros=0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_DELETE);
            sen.setString(1, tp.getIdtarea_participante());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace(System.err);
        }
        finally{
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
            
        }
        
     return registros;
    }
    
    
    
    
}
