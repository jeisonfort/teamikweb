/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.ParticipanteDTO;
import Modelo.ProyectoDTO;
import Modelo.TareaDTO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author jeiso
 */
public interface TareaDAO {
    
    public int crear(TareaDTO tarea, ProyectoDTO proyecto);
    public List<TareaDTO> consultar();
    public int update (TareaDTO t);
    public int delete (TareaDTO t);
    public List<TareaDTO> buscarTareas(int idP)  ;
}
