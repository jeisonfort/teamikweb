/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.ParticipanteDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guillermo
 */
public class ParticipanteDAOJDBC implements ParticipanteDAO {

    public static final String SQL_INSERTAR = "INSERT INTO participante (codigo,idequipo,nombre,idusuario,contrasena,correo) VALUES (?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT codigo,idequipo,nombre,idusuario,contrasena,correo FROM participante";
    public static final String SQL_UPDATE = " UPDATE participante SET idequipo=?,nombre=?,idusuario=?,contrasena=?,correo=? WHERE codigo =? ";
    public static final String SQL_DELETE = " DELETE FROM participante WHERE codigo=?";
    public static final String SQL_VERIFICAR = " SELECT correo,contrasena FROM participante ";
    public static final String SQL_CONSULTA_BY_ID = "SELECT idequipo,nombre,idusuario,contrasena,correo FROM participante WHERE codigo= ?";
    public static final String SQL_CONSULTA_LOGIN = "SELECT codigo,idequipo,nombre,idusuario,contrasena FROM participante WHERE correo= ?";
    public int crear(ParticipanteDTO p) {

        Connection con = null;
        PreparedStatement sen = null;
        int registros = 0;

        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_INSERTAR);
            sen.setInt(1, p.getCodigo());
            sen.setInt(2, p.getIdequipo());
            sen.setString(3, p.getNombre());
            sen.setString(4, p.getIdusuario());
            sen.setString(5, p.getContrasena());
            sen.setString(6, p.getCorreo());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }

        }
        return registros;
    }

    /**
     *
     * @return @throws SQLException
     */
    @Override
    public List<ParticipanteDTO> consultar()  {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        ParticipanteDTO participante = null;
        List<ParticipanteDTO> participantes = new ArrayList<>();
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA);
            res = sen.executeQuery();

            while (res.next()) {
                int codigo = res.getInt("codigo");
                int idquipo = res.getInt("idequipo");
                String nombre = res.getString("nombre");
                String idusuario = res.getString("idusuario");
                String contrasena = res.getString("contrasena");
                String correo = res.getString("correo");

                participante = new ParticipanteDTO(codigo, idquipo, nombre, idusuario, contrasena, correo);
                participantes.add(participante);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return participantes;
    }

    public int actualizar(ParticipanteDTO p) {
        Connection con = null;
        PreparedStatement sen = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_UPDATE);
            sen.setInt(1, p.getIdequipo());
            sen.setString(2, p.getNombre());
            sen.setString(3, p.getIdusuario());
            sen.setString(4, p.getContrasena());
            sen.setString(5, p.getCorreo());
            sen.setInt(6, p.getCodigo());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }

        }

        return registros;
    }

    public int eliminar(ParticipanteDTO p) {
        Connection con = null;
        PreparedStatement sen = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_DELETE);
            sen.setInt(1, p.getCodigo());
            registros = sen.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                ConexionBD.close(sen);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }

        }

        return registros;
    }
    @Override
    public boolean verificar(String correo, String contrasena) {
        boolean verificado = false;
        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        ParticipanteDTO participante = new ParticipanteDTO(0, 0, correo, correo, contrasena, correo);
        
        try {
            List<ParticipanteDTO> participantes = consultar();
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_VERIFICAR);
            res = sen.executeQuery();
           
            for (ParticipanteDTO participante1 : participantes) {
                
              if(participante1.getCorreo().equals(participante.getCorreo()) && participante1.getContrasena().equals(participante.getContrasena())){
                
                 return verificado = true;
                
              }
            
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            
            return verificado;
        }
     /**
     *
     * @return @throws SQLException
     */
    @Override
    public ParticipanteDTO buscarLogin(ParticipanteDTO p)  {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA_LOGIN);
            sen.setString(1, p.getCorreo());
            res = sen.executeQuery();
            System.out.println("P:"+p.getCorreo()+ " : "+p.getContrasena());
            if(!res.next()|| !p.getContrasena().equals(res.getString("contrasena"))) 
            {    
                p.setCorreo("INVALIDO");
                return p;            
            }
            
           int idquipo = res.getInt("idequipo");
           String nombre = res.getString("nombre");
           String idusuario = res.getString("idusuario");
           String contrasena = res.getString("contrasena");
           int codigo = res.getInt("codigo");
           
           p.setCodigo(codigo);
           p.setNombre(nombre);
           p.setContrasena(contrasena);
           p.setIdequipo(idquipo);
           p.setIdusuario(idusuario);        
                
            
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return p;
    }
     /**
     *
     * @return @throws SQLException
     */
    @Override
    public ParticipanteDTO buscar(ParticipanteDTO p)  {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA_BY_ID);
            sen.setInt(1, p.getCodigo());
            res = sen.executeQuery();
            res.next();
           
                int idquipo = res.getInt("idequipo");
                String nombre = res.getString("nombre");
                String idusuario = res.getString("idusuario");
                String contrasena = res.getString("contrasena");
                String correo = res.getString("correo");
                p.setNombre(nombre);
                p.setCorreo(correo);
                p.setContrasena(contrasena);
                p.setIdequipo(idquipo);
                p.setIdusuario(idusuario);        
                
            
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return p;
    }
    }


