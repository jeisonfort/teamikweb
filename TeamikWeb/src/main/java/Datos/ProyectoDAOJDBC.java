/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import static Datos.ParticipanteDAOJDBC.SQL_CONSULTA_LOGIN;
import Modelo.ParticipanteDTO;
import Modelo.ProyectoDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Sebastian
 */
public class ProyectoDAOJDBC implements ProyectoDAO{

    public static final String SQL_CONSULTA = "SELECT idproyecto, nombre, fechainicio, fechafinal FROM proyecto";
    public static final String SQL_INSERTAR = "INSERT INTO proyecto VALUES (?,?,?,?)";
    public static final String SQL_ACTUALIZAR = "UPDATE proyecto SET nombre = ?, fechainicio = ?, fechafinal = ? WHERE idproyecto = ?";
    public static final String SQL_ELIMINAR = "DELETE FROM proyecto WHERE idproyecto = ?";
    public static final String SQL_CONSULTA_BY_ID = "SELECT  nombre, fechainicio, fechafinal FROM proyecto WHERE idproyecto= ?";

    public List<ProyectoDTO> consultar() {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet res = null;
        ProyectoDTO proyecto = null;
        List<ProyectoDTO> proyectos = new ArrayList<>();
        try {
            con = ConexionBD.getConnection();
            stmt = con.prepareStatement(SQL_CONSULTA);
            res = stmt.executeQuery();
            while (res.next()) {
                int idproyecto = res.getInt("idproyecto");
                String nombre = res.getString("nombre");
                Date fechaInicio = res.getDate("fechainicio");
                Date fechaFinal = res.getDate("fechafinal");
                proyecto = new ProyectoDTO(idproyecto, nombre, fechaInicio, fechaFinal);
                proyectos.add(proyecto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProyectoDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(stmt);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(ProyectoDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return proyectos;
    }

    public int crear(ProyectoDTO proyecto) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            stmt = con.prepareStatement(SQL_INSERTAR);
            stmt.setInt(1, proyecto.getIdproyecto());
            stmt.setString(2, proyecto.getNombre());
            stmt.setDate(3, proyecto.getFechaInicio());
            stmt.setDate(4, proyecto.getFechaFinal());
            registros = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(stmt);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    public int actualizar(ProyectoDTO proyecto) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            stmt = con.prepareStatement(SQL_ACTUALIZAR);
            stmt.setString(1, proyecto.getNombre());
            stmt.setDate(2, proyecto.getFechaInicio());
            stmt.setDate(3, proyecto.getFechaFinal());
            stmt.setInt(4, proyecto.getIdproyecto());
            registros = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(stmt);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    public int eliminar(ProyectoDTO proyecto) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            stmt = con.prepareStatement(SQL_ELIMINAR);
            stmt.setInt(1, proyecto.getIdproyecto());
            registros = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(stmt);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }
    @Override
    public ProyectoDTO buscar(ProyectoDTO p)  {

        Connection con = null;
        PreparedStatement sen = null;
        ResultSet res = null;
        try {
            con = ConexionBD.getConnection();
            sen = con.prepareStatement(SQL_CONSULTA_BY_ID);
            sen.setInt(1, p.getIdproyecto());
            res = sen.executeQuery();
           // System.out.println("P:"+p.getCorreo()+ " : "+p.getContrasena());
            if(!res.next()) 
            {    
                p.setNombre("INVALIDO");
                return p;            
            }
    
                String nombre = res.getString("nombre");
                Date fechaInicio = res.getDate("fechainicio");
                Date fechaFinal = res.getDate("fechafinal");
               
           p.setNombre(nombre);
           p.setFechaInicio(fechaInicio);
           p.setFechaFinal(fechaFinal);
       
                
            
        } catch (SQLException ex) {
            Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(sen);
                ConexionBD.close(con);

            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return p;
    }

}
