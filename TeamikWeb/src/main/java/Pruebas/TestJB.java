/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pruebas;

import Datos.ProyectoDAO;
import Datos.ProyectoDAOJDBC;
import Datos.TareaDAO;
import Datos.TareaDAOJDBC;
import Modelo.ProyectoDTO;
import Modelo.TareaDTO;
import java.sql.Date;

/**
 *
 * @author jeiso
 */
public class TestJB {
     public static void main (String arg[]){
        //PRUEBA DE CONeXION PERFECT
        TareaDAO tarea = new TareaDAOJDBC();
        TareaDTO nuva = new TareaDTO();
        ProyectoDAOJDBC p = new ProyectoDAOJDBC();
        nuva.setNombre("Prueba Relacion 2");
        nuva.setEntregada(true);
        nuva.setActiva(true);
        nuva.setFechaInicio(new Date(2020-1900, 10, 1));
        nuva.setFechaFin(new Date (2020-1900,10,2));
        nuva.setEvidencia("AASASAS");
        tarea.crear(nuva,p.consultar().get(0));
        for (TareaDTO t : tarea.consultar()) {
            System.out.println(t.toString());
        }
    }
}
