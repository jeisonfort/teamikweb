/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author jeiso
 */
public class ProyectoDTO {
    private int idproyecto;
    private String nombre;
    private Date fechaInicio;
    private Date fechaFinal;

    public ProyectoDTO() {
    }

    public ProyectoDTO(int idproyecto) {
        this.idproyecto = idproyecto;
    }
    
    public ProyectoDTO(int idproyecto, String nombre, Date fechaInicio, Date fechaFinal) {
        this.idproyecto = idproyecto;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFinal = fechaFinal;
    }

    public int getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(int idproyecto) {
        this.idproyecto = idproyecto;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }


    @Override
    public String toString() {
        return "Proyecto{" + "idproyecto=" + idproyecto + ", nombre=" + nombre + ", fechaInicio=" + fechaInicio + ", fechaFinal=" + fechaFinal + '}';
    }
    
    
    
}
