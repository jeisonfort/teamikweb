/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author jeiso
 */
public class ParticipanteDTO {
    private int codigo;
    private int idequipo;
    private String nombre;
    private String idusuario;
    private String contrasena;
    private String correo;

    public ParticipanteDTO() {
    }

    public ParticipanteDTO(int codigo, int idequipo, String nombre, String idusuario, String contrasena, String correo) {
        this.codigo = codigo;
        this.idequipo = idequipo;
        this.nombre = nombre;
        this.idusuario = idusuario;
        this.contrasena = contrasena;
        this.correo = correo;
    }

 
    
    public ParticipanteDTO(String correo, String contrasena) {
        this.correo = correo;
        this.contrasena = contrasena;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(int idequipo) {
        this.idequipo = idequipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "Participante{" + "codigo=" + codigo + ", idequipo=" + idequipo + ", nombre=" + nombre + ", idusuario=" + idusuario + ", contrasena=" + contrasena + ", correo=" + correo + '}';
    }

    
    
    
}
