/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.ParticipanteDTO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Guillermo
 */
public interface ParticipanteDAO {
    
    
    public List<ParticipanteDTO> consultar() ;
    public int crear(ParticipanteDTO p);
    public int actualizar(ParticipanteDTO p);
    public int eliminar(ParticipanteDTO p);
    public boolean verificar(String correo, String contraseña);
    public ParticipanteDTO buscar(ParticipanteDTO p) ;
    public ParticipanteDTO buscarLogin(ParticipanteDTO p) ;
}
