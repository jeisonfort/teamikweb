<%-- 
    Document   : index
    Created on : 19/12/2020, 8:45:35 p. m.
    Author     : jeiso
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Teamik</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="css/coming-soon.min.css" rel="stylesheet">

</head>

<body>

  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="mp4/bg.mp4" type="video/mp4">
  </video>

  <div class="masthead">
    <div class="masthead-bg"></div>
    <div class="container h-100" >
      <div class="row h-100">
        <div class="col-12 my-auto">
          <div class="masthead-content text-white py-5 py-md-0">
            <h1 class="mb-3">Bienvenido a Teamik!</h1>
            <p class="mb-2">Con nuestra plataforma puedes administrar tus proyectos facilmente
                ! Registrate y comienza tu proyecto!!</p>
            <div class="mb-2">
                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                        <input type="text" name ="nombre" class="form-control" id="recipient-name" required>
                    </div>
            <div class="mb-2">
                        <label for="recipient-name" class="col-form-label">Apellido:</label>
                        <input type="text" name ="apellido" class="form-control" id="recipient-name" required>
                    </div>
            <div class="mb-2">
                        <label for="recipient-name" class="col-form-label">Correo:</label>
                        <input type="text" name ="correo" class="form-control" id="recipient-name" required>
                    </div>
            <div class="mb-2">
                        <label for="recipient-name" class="col-form-label">Clave:</label>
                        <input type="password" name ="clave" class="form-control" id="recipient-name" required>
                       
                    </div>
            <div class="input-group input-group-newsletter">
              <div class="input-group-append">
                  <button class="btn btn-primary" onclick="location.href='dist/login.jsp'" type="button" id="submit-button">Iniciar sesion</button>
              </div>
              <div class="input-group-append">
                <button class="btn btn-primary" onclick="location.href='dist/login.jsp'" type="button" id="submit-button">Registrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="social-icons">
    <ul class="list-unstyled text-center mb-0">
      <li class="list-unstyled-item">
        <a href="#">
          <i class="fab fa-twitter"></i>
        </a>
      </li>
      <li class="list-unstyled-item">
        <a href="#">
          <i class="fab fa-facebook-f"></i>
        </a>
      </li>
      <li class="list-unstyled-item">
        <a href="#">
          <i class="fab fa-instagram"></i>
        </a>
      </li>
    </ul>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/coming-soon.min.js"></script>

</body>

</html>

