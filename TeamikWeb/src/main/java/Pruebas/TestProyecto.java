/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pruebas;

import Datos.ProyectoDAOJDBC;
import Modelo.ProyectoDTO;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public class TestProyecto {

    public static void main(String[] args) {
        ProyectoDAOJDBC proyecto = new ProyectoDAOJDBC();
        ProyectoDTO pro = new ProyectoDTO();
        pro.setNombre("Prueba crear 2");
        pro.setFechaInicio(new Date(2010-1900,3,2));
        pro.setFechaFinal(new Date(2015-1900,5,20));

       //proyecto.crear(pro);

        List<ProyectoDTO> proyectos = new ArrayList<>();

        proyectos = proyecto.consultar();
        for (ProyectoDTO proyec : proyectos) {
            System.out.println(proyec);
        }
        System.out.println("****************************");
        System.out.println(proyecto.buscar(new ProyectoDTO(181127)).toString());
        
    }
}
