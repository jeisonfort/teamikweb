/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Proyecto_ParticipanteDTO;
import java.util.List;

/**
 *
 * @author Guillermo
 */
public interface Proyecto_ParticipanteDAO {
    public List<Proyecto_ParticipanteDTO> consultar();
    public int crear(Proyecto_ParticipanteDTO pp);
    public int actualizar(Proyecto_ParticipanteDTO pp);
    public int eliminar(Proyecto_ParticipanteDTO pp);
}
