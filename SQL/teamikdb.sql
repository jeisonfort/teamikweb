-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-12-2020 a las 16:18:11
-- Versión del servidor: 10.2.10-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `teamikdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE `participante` (
  `codigo` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `idusuario` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`codigo`, `idequipo`, `nombre`, `idusuario`, `contrasena`, `correo`) VALUES
(124253, 23, 'KAKA', '10023', 'Contraseña', 'Kaka@gmail.com'),
(124254, 23, 'Prueba crar participante', '10023', '123456789', 'prueba19@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idproyecto` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `fechainicio` datetime NOT NULL,
  `fechafinal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`idproyecto`, `nombre`, `fechainicio`, `fechafinal`) VALUES
(181127, 'TeamikJB', '3920-11-01 00:00:00', '3921-11-02 00:00:00'),
(181227, 'Teamik', '3920-11-01 00:00:00', '3921-11-02 00:00:00'),
(181247, 'Ejemplo', '1908-11-10 00:00:00', '1917-01-11 00:00:00'),
(181257, 'Proyecto', '1914-10-31 00:00:00', '1907-08-04 00:00:00'),
(191127, 'Empresa', '1926-05-04 00:00:00', '1927-06-04 00:00:00'),
(12422346, 'Cucuta', '2010-04-02 00:00:00', '2015-06-20 00:00:00'),
(12422347, 'Prueba crear', '2010-04-02 00:00:00', '2015-06-20 00:00:00'),
(12422348, 'Prueba crear 2', '2010-04-02 00:00:00', '2015-06-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto_participante`
--

CREATE TABLE `proyecto_participante` (
  `idproyectoparticipante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `codigoparticipante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `idtarea` int(11) NOT NULL,
  `fechainicio` datetime NOT NULL,
  `fechafin` datetime NOT NULL,
  `entregada` tinyint(1) NOT NULL,
  `activa` tinyint(1) NOT NULL,
  `evidencia` text COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `idproyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`idtarea`, `fechainicio`, `fechafin`, `entregada`, `activa`, `evidencia`, `nombre`, `idproyecto`) VALUES
(1235, '1907-05-13 00:00:00', '1908-05-13 00:00:00', 1, 1, 'AASASAS', 'Prueba', 0),
(1236, '1907-05-13 00:00:00', '1908-05-13 00:00:00', 1, 1, 'AASASAS', 'Prueba Relacion', 124253),
(1237, '3920-11-01 00:00:00', '3920-11-02 00:00:00', 1, 1, 'AASASAS', 'Prueba Relacion', 124253),
(1238, '2020-11-01 00:00:00', '2020-11-02 00:00:00', 1, 1, 'AASASAS', 'Prueba Relacion 2', 124253),
(1239, '2020-11-01 00:00:00', '2020-11-02 00:00:00', 1, 1, 'AASASAS', 'Prueba Relacion 3', 124253),
(1240, '2020-11-01 00:00:00', '2020-11-02 00:00:00', 0, 1, 'AASASAS', 'Prueba Relacion 3', 181127),
(1241, '2020-11-01 00:00:00', '2020-11-02 00:00:00', 0, 1, 'AASASAS', 'Prueba Relacion 3', 181127);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_participante`
--

CREATE TABLE `tarea_participante` (
  `idtareaparticipante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `idtarea` int(11) NOT NULL,
  `codigoparticipante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tarea_participante`
--

INSERT INTO `tarea_participante` (`idtareaparticipante`, `idtarea`, `codigoparticipante`) VALUES
('1235-10023', 12321, 10023);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idproyecto`);

--
-- Indices de la tabla `proyecto_participante`
--
ALTER TABLE `proyecto_participante`
  ADD PRIMARY KEY (`idproyectoparticipante`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`idtarea`);

--
-- Indices de la tabla `tarea_participante`
--
ALTER TABLE `tarea_participante`
  ADD PRIMARY KEY (`idtareaparticipante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124255;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idproyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12422349;

--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `idtarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1242;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
